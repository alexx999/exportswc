﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExportSWC.Tracing
{
	public enum TraceMessageType
	{
		Verbose = 0,
		Message = 1,
		Warning = 2,
		Error = 3,
		Fatal = 4
	}
}
